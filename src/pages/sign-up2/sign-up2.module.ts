import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignUp2Page } from './sign-up2';

@NgModule({
  declarations: [
    SignUp2Page,
  ],
  imports: [
    IonicPageModule.forChild(SignUp2Page),
  ],
})
export class SignUp2PageModule {}
