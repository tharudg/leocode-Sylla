import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { SignUp2Page } from '../pages/sign-up2/sign-up2';
import { AccountPage } from '../pages/account/account';
import { MyCoursesPage } from '../pages/my-courses/my-courses';
import { SubjectsPage } from '../pages/subjects/subjects';
import { DashboardPage } from '../pages/dashboard/dashboard';

import * as firebase from 'firebase';
import { MainStore } from '../store/main.store';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any =LoginPage;

  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public store: MainStore
  
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'login', component: LoginPage},
      { title: 'sign-up', component: SignUpPage},
      { title: 'sign-up2', component: SignUp2Page},
      { title: 'account', component: AccountPage},
      { title: 'my-courses', component: MyCoursesPage},
      { title: 'subjects', component: SubjectsPage},
      { title: 'dashboard', component:DashboardPage},
      
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      firebase.initializeApp({
        apiKey: "AIzaSyBOgLX5OEtuwJslgn9ARCFAQxpQ47yuU7A",
        authDomain: "sylla-d2508.firebaseapp.com",
        databaseURL: "https://sylla-d2508.firebaseio.com",
        projectId: "sylla-d2508",
        storageBucket: "sylla-d2508.appspot.com",
        messagingSenderId: "1069633253896"
      })


        firebase.database().ref('subjets').on('value', (res)=>{
          
          let data =  res.val();
          console.log(data)
          let ids = Object.keys(data);
          this.store.subjets =  new Array<any>();
          for (let id of ids){
            this.store.subjets.push(data[id]);
          }
         })
      
      
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page);
  }
  openDashboard(){
    this.nav.push(DashboardPage);
  }
  openHome(){
    this.nav.push(HomePage);
  }
  openAccount(){
    this.nav.push(AccountPage);
  }
  openCourses(){
    this.nav.push(MyCoursesPage,{
      title: "My Courses"
    });
  }
}
