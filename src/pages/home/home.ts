import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';

import * as firebase from 'firebase';
import { MainStore } from '../../store/main.store';
import { MyCoursesPage } from '../my-courses/my-courses';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(
    public navCtrl: NavController,
    public store: MainStore, 
    public platform:Platform) {




    
  }


  btnClick(id, title){
    this.navCtrl.push(MyCoursesPage,{
      title: title
    } )

  }

}
